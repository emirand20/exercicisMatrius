let rows = document.querySelector("tbody").children
let matrix = []
for (let i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}
/*let matrix = [
    1, 2, 3,
    4, 5, 6,
    7, 8, 9
]
*/

/** 
 * i recorrera todas keys de izquierda a derecha hasta que i sea igual a 9
 * j recorrera cada una de las keys de arriba abajo comenzando por la 1 y siguiendo por la 4. 
 * de manera que cada vez que sea verdadero estos bucles pintara de blanco cuando estos dos sean verdaderos.
 */
function erase() {
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0; j < matrix[i].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

/**
 * la misma funcion anterior pero pintandola de rojo
 */
function paintAll() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0; j < matrix[i].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

/**
 * j -> queremos pintar de rojo la mitad de la izq, asi que la dividiremos por la mirad j
 */
function paintLeftHalf() {
    erase();

    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0; j < matrix[i].length / 2; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

/**
 * j -> establereceremos que j empiece desde el final y acabara hasta la mitad vertical
 */
function paintRightHalf() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = matrix[i].length - 1; j > matrix[i].length / 2; j--) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}


function paintUpperHalf() {
    erase();

    for (let i = 0; i < matrix.length / 2; i++) { // afegir codi
        for (let j = 0; j < matrix.length / 2 + 2; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (i > j) {
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintUpperTriangle() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j = 0; j < matrix[i].length; j++) { // afegir codi
            if (i <= j) {
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }

}

function paintPerimeter() {
    erase();
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) { // afegir codi
            if (i == 0 || j == 0 || i == matrix.length - 1 || j == matrix[i].length - 1) { // afegir codi
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintCheckerboard() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        let startNumb = i % 2 == 0 ? 0 : 1
        for (let j = startNumb; j < matrix[i].length; j = j + 2) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintCheckerboard2() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        let startNumb = i % 2 == 0 ? 1 : 0
        for (let j = startNumb; j < matrix[i].length; j = j + 2) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}
function paintNeighbours() {
    let inputX = document.getElementById("inputX").valueAsNumber;
    let inputY = document.getElementById("inputY").valueAsNumber;
    
    for () {
        for () {
     mes coses ...
    }
    }
    /
}

/*function countNeighbours(x,y) {
    let count = 0;
    
    for () {
        for () {
     mes coses ...
    }
    }
    
    return count;
}*/

/*function paintAllNeighbours() {
    
    for () {
        for () {
        matrix[i][j].innerText = count;
    }
    }
    
}*/